import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

corr_df = pd.read_csv("rated_subset_merged.csv")

#Produce correlation heatmap
corr_plot = corr_df.corr()
mask = np.triu(np.ones_like(corr_plot, dtype=bool))
f, ax = plt.subplots(figsize=(11, 9))
cmap = sns.diverging_palette(120, 250, s=100, as_cmap=True)
sns.set(rc = {'figure.figsize': (20,20), 'axes.labelsize': 12})
sns.heatmap(corr_plot, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5}).get_figure().savefig("correlation_matrix.png")


