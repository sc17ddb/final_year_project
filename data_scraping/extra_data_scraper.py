import requests, json, time
import pandas as pd

id_csv = pd.read_csv("video_id_scraper_output.csv")

api_keys = ['AIzaSyBApb8BsFWW2B3NUPQfSacpyqDFh-oHTi4']

ids = []
thumbnail_urls = []
tags = []
category_ids = []
durations = []
captions = []
views = []
likes = []
dislikes = []
comments = []

count = 0
api_count = 0
# Iterate over video IDs
for v_id in id_csv['id']:


    print("{}% done".format((count/len(id_csv['id']))*100))

    vid_request = requests.get("https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Csnippet%2Cstatistics&id={0}&maxResults=50&key={1}".format(v_id, api_keys[api_count]))

    vid_json = vid_request.json()

    item_len = len(vid_json['items'])
    # Check video actually exists
    if item_len == 1:
        failure = False
        # Some items may not be included, handle these as appropriate
        try:
            thumbnail_url = vid_json['items'][0]['snippet']['thumbnails']['default']['url']
        except KeyError:
            failure = True
        try:
            tag = vid_json['items'][0]['snippet']['tags']
        except KeyError:
            tag = []
        try:
            category_id = vid_json['items'][0]['snippet']['categoryId']
        except KeyError:
            category_id = -1
        try:
            duration = vid_json['items'][0]['contentDetails']['duration']
        except KeyError:
            duration = -1
        try:
            caption = vid_json['items'][0]['contentDetails']['caption']
        except KeyError:
            caption = False
        try:
            view = vid_json['items'][0]['statistics']['viewCount']
        except KeyError:
            view = -1
        try:
            like = vid_json['items'][0]['statistics']['likeCount']
        except KeyError:
            like = -1
        try:
            dislike = vid_json['items'][0]['statistics']['dislikeCount']
        except KeyError:
            dislike = -1
        try:
            comment = vid_json['items'][0]['statistics']['commentCount']
        except KeyError:
            comment = -1
        # Add in all the data we scraped
        if not failure:
            thumbnail_urls.append(thumbnail_url)
            tags.append(tag)
            category_ids.append(category_id)
            durations.append(duration)
            captions.append(caption)
            views.append(view)
            likes.append(like)
            dislikes.append(dislike)
            comments.append(comment)
            ids.append(v_id)
    # Skip non unique ID videos (should not really happen)
    elif item_len == 0:
        f = input("Video with id {} not found - press enter to skip it".format(v_id))
    else:
        f = input("Multiple videos found with id {} - press enter to skip it".format(v_id))
    count = count + 1
    api_count = api_count + 1
    if api_count > len(api_keys) - 1:
        api_count = 0
    # Don't overload API
    time.sleep(0.1)
# Output data
output_file = open("extra_data_scraper_output.csv", "w", encoding='utf-8')
output_file.write('id,thumbnail_url,tags,category_id,duration,captions,views,likes,dislikes,comments')
output_file.write('\n')
for i in range(len(ids)):
    if '"' in ids[i]:
        f = input("FATAL: VIDEO ID CONTAINING QUOTE, Video with id {0} can be skipped with enter".format(ids[i]))
    elif '"' in thumbnail_urls[i]:
        f = input("Video with id {0} can be skipped with enter due to quote in the url to thumbnail".format(thumbnail_urls[i]))
    else:
        output_file.write('"{0}","{1}","{2}",{3},"{4}",{5},{6},{7},{8},{9}'.format(ids[i], thumbnail_urls[i], str(tags[i]), category_ids[i], durations[i], captions[i], views[i], likes[i], dislikes[i], comments[i]))
        output_file.write('\n')
output_file.close()