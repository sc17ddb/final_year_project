import pandas as pd
import numpy as np
import gensim
from gensim.utils import simple_preprocess
import nltk
nltk.download('stopwords')
import gensim.corpora as corpora
from nltk.corpus import stopwords
from pprint import pprint
from wordcloud import WordCloud

stop_words = stopwords.words('english')
results = pd.read_csv("../data/temp_results.csv")
subset = pd.read_csv("subset_merged.csv")

def corpus_to_words(arr_corpus):
    for sentence in arr_corpus:
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))

def clean_stops(to_clean):
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in to_clean]

# Clean titles
titles = subset['title'].to_numpy()
cleanedTitles = [x for x in titles if str(x) != 'nan']

# Produce word clouds
all_titles = ','.join(cleanedTitles)
wordcloud = WordCloud(background_color="white", max_words=500, contour_width=3, contour_color='steelblue')
wordcloud.generate(all_titles)
wordcloud.to_file("word_cloud.png")

# Produce corpus
data_words = list(corpus_to_words(cleanedTitles))
data_words = remove_stopwords(data_words)
id2word = corpora.Dictionary(data_words)
corpus = [id2word.doc2bow(text) for text in data_words]

# Produce topic model
lda_model = gensim.models.ldamodel.LdaModel(corpus, id2word=id2word)
pprint(lda_model.print_topics())
doc_lda = lda_model[corpus]

