import requests, json, time

# Edit PL (Playlist ID) as required
key = 'AIzaSyBApb8BsFWW2B3NUPQfSacpyqDFh-oHTi4'
PL = 'UUBNG0osIBAprVcZZ3ic84vw'

v_id_request = requests.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=50&playlistId={0}&key={1}".format(PL, key))

v_id_json = v_id_request.json()

item_len = len(v_id_json['items'])

titles = []
descs = []
ids = []
times = []

# Go through all videos in the uploads playlist
while item_len > 0:
    try:
        next_token = v_id_json['nextPageToken']
    except KeyError:
        break

    for video in v_id_json['items']:
        titles.append(video['snippet']['title'])
        descs.append(video['snippet']['description'])
        ids.append(video['contentDetails']['videoId'])
        times.append(video['snippet']['publishedAt'])
    
    #Request basic info about this video
    v_id_request = requests.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=25&pageToken={0}&playlistId={1}&key={2}".format(next_token, PL, key))

    v_id_json = v_id_request.json()

    try:
        item_len = len(v_id_json['items'])
    except KeyError:
        item_len = 0
    # Don't overload API
    time.sleep(0.1)

# Output Information found
output_file = open("video_id_scraper_output.csv", "w", encoding='utf-8')
output_file.write('id,title,description,time')
output_file.write('\n')
for i in range(len(ids)):
    if '"' in ids[i]:
        f = input("FATAL: VIDEO ID CONTAINING QUOTE, Video with id {0} and title {1} can be skipped with enter".format(ids[i], titles[i]))
    else:
        output_file.write('"{0}","{1}","{2}","{3}"'.format(ids[i], titles[i].replace('"', ""), descs[i].replace('"', ""), times[i].replace('"', "")))
        output_file.write('\n')
output_file.close()