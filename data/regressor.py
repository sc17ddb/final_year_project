import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LarsCV
from sklearn.neural_network import MLPRegressor
from gensim.models import Word2Vec

# Open the files
results = pd.read_csv("temp_results.csv")
subset = pd.read_csv("subset.csv")

# Cleanse titles and produce Word2Vec Model
titles = subset['title'].to_numpy()
cleanedTitles = [x for x in titles if str(x) != 'nan']
word2vecArray = []
for x in cleanedTitles:
    temp_list = []
    for y in x.split():
        temp_list.append(y)
    word2vecArray.append(temp_list)
model = Word2Vec(word2vecArray, min_count=1)

# Produce standard length vectors for each title by converting the words in it
max_len = 1200
X = []
y = []
for vid_id in results['id']:
    vid_title = subset.loc[subset['id'] == vid_id]['title'].to_string(index=False)[1:]
    if vid_title in cleanedTitles:
        vector_word2vec = []
        for word in vid_title.split():
            vector_word2vec.extend(model[word])
        if len(vector_word2vec) < max_len:
            zeros_to_add = max_len - len(vector_word2vec)
            array_zeros_to_add = np.zeros(zeros_to_add)
            vector_word2vec.extend(array_zeros_to_add)
        # Find the score corresponding to that vector
        score = results.loc[results['id'] == vid_id]['title_score'].values[0]
        X.append(vector_word2vec)
        y.append(score)

# Train and evaluate the model
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
reg = MLPRegressor(random_state=1, max_iter=500).fit(X_train, y_train)
print("Score: {}".format(reg.score(X_test, y_test)))
index = 0
for x in X_test:
    f = input("Predicted score: {0}\nReal Score:{1}\n".format(reg.predict(np.array(x).reshape(1, -1)), y_test[index]))
    index = index + 1