import Vue from 'vue'
import App from './App.vue'
import VuePapaParse from 'vue-papa-parse'
import vuetify from '@/plugins/vuetify'

Vue.use(VuePapaParse)
Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App),
}).$mount('#app')
