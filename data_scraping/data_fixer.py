import pandas as pd

fix_me = open("extra_data_scraper_output.csv", "r", encoding='utf-8')

fixed = open("extra_data_scraper_output_fixed.csv", "w", encoding='utf-8')

fixed.write(fix_me.readline())

while True:
    line = fix_me.readline()
    if not line:
        break
    #locate the [
    first_bracket = line.find("[")
    #locate the ]
    second_bracket = line.find("]")
    #replace every ', ' with a tab
    substring_og = line[first_bracket:second_bracket+1]
    substring_rp = '"' + substring_og.replace(", ", "\t") + '"'

    #sub the new line back in
    line = line.replace(substring_og, substring_rp)
    fixed.write(line)

fix_me.close()
fixed.close()

