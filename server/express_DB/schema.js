const mongoose = require('mongoose');

let ratingSchema = mongoose.Schema({
  0: {type: Number, default: 0},
  1: {type: Number, default: 0},
  2: {type: Number, default: 0},
  3: {type: Number, default: 0},
  4: {type: Number, default: 0},
  5: {type: Number, default: 0},
  6: {type: Number, default: 0},
  7: {type: Number, default: 0},
  8: {type: Number, default: 0},
  9: {type: Number, default: 0},
  10: {type: Number, default: 0}
}, {minimize: false})

let resultsSchema = mongoose.Schema({
  id: String,
  rowNumber: Number,
  thumbnail: {
    type: Object
  },
  title: {
    type: Object
  }
}, {minimize: false});

module.exports = mongoose.model('Result', resultsSchema, 'results');
