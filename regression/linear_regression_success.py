import pandas as pd
import numpy as np
import gensim
from gensim.utils import simple_preprocess
import nltk
nltk.download('stopwords')
nltk.download('punkt')
import gensim.corpora as corpora
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
import seaborn as sns

def id_to_title(vid_id):
    return subset.loc[subset['id'] == vid_id]['title'].to_string(index=False)[1:]

results = pd.read_csv("temp_results.csv")
subset = pd.read_csv("subset_merged.csv")
stop_words = stopwords.words('english')

# Cleanse data
titles_rated = [id_to_title(vid_id) for vid_id in results['id']]
titles = subset['title']
scores = subset['success_rating']
cleanedTitles = [x for x in titles if str(x) != 'nan']
cleanedTitlesRated = []
cleanedScores = []
index = 0
for x in titles_rated:
    if str(x) != 'nan':
        cleanedTitlesRated.append(x)
        cleanedScores.append(scores[index])
    index = index + 1

# Reformat data
tokenized_sent = []
for s in cleanedTitles:
    tokenized_sent.append(word_tokenize(s.lower()))
tagged_data = [TaggedDocument(d, [i]) for i, d in enumerate(tokenized_sent)]

# Run model over multiple epcjs
reg_scores = []
arr_epochs = [10, 20, 50, 100]
count = 0
for epochs in arr_epochs:
    count = count + 1
    print("{0}/{1}".format(count, len(arr_epochs)))
    model = Doc2Vec(tagged_data, vector_size = 10, window = 2, min_count = 1, epochs = epochs)

    X = []
    for ratedTitle in cleanedTitlesRated:
        tokenized_sent = word_tokenize(ratedTitle.lower())
        doc_vec = model.infer_vector(tokenized_sent)
        X.append(doc_vec)
    y = cleanedScores

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    reg = LinearRegression().fit(X_train, y_train)
    sns.regplot(x=X_train, y=y_train).get_figure().savefig("replot_{0}.png".format(test_size))
    reg_scores.append(reg.score(X_test, y_test))

# Evaluate model
print(reg_scores)
ax = sns.regplot(x=arr_epochs, y=reg_scores)
ax.set_xlabel("Epochs")
ax.set_ylabel("Score")
ax.get_figure().savefig("regplot_epochs_on_success_rating.png")

