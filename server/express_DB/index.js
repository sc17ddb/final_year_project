const express = require("express")
const cors = require("cors")
const morgan = require("morgan")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const converter = require("number-to-words")

const app = express()

app.use(morgan('tiny'));
app.use(cors());
app.use(bodyParser.json());

const MONGO_URI = "mongodb://root:!OX8zlMC88*o7x@mongo:27017/results?authSource=admin"
mongoose.connect(MONGO_URI, { useNewUrlParser: true, useFindAndModify: false });

const Result = require("./schema.js")

app.post("/store", async(req, res) => {
  //console.log(req.body)
  let id = req.body.id, value = req.body.value, type = req.body.type, rowNum = req.body.rowNumber
  let v = ""
  if(type == "thumbnail"){
    v = 'thumbnail.' + value
  } else {
    v = 'title.' + value
  }

  await Result.findOneAndUpdate({id: id}, {$setOnInsert: {rowNumber: rowNum, thumbnail: {"0":0, "1":0, "2":0, "3":0, "4":0, "5":0, "6":0, "7":0, "8":0, "9":0, "10":0}, title: {"0":0, "1":0, "2":0, "3":0, "4":0, "5":0, "6":0, "7":0, "8":0, "9":0, "10":0}}}, {upsert: true})
  Result.findOneAndUpdate({id: id}, {$inc: {[`${v}`]: 1}}, {new: true}, (err, response) => {err ? console.log(err) : res.send(response)})
  //console.log(v)
})

app.get("/lookup", async(req, res) => {
  let readyToSend = await Result.find({
    $or : [
      {
        'thumbnail.0': 0, 
        'thumbnail.1': 0, 
        'thumbnail.2': 0, 
        'thumbnail.3': 0, 
        'thumbnail.4': 0, 
        'thumbnail.5': 0, 
        'thumbnail.6': 0, 
        'thumbnail.7': 0, 
        'thumbnail.8': 0, 
        'thumbnail.9': 0, 
        'thumbnail.10': 0,
        $or : [
          {'title.0': {$ne: 0}}, 
          {'title.1': {$ne: 0}}, 
          {'title.2': {$ne: 0}}, 
          {'title.3': {$ne: 0}}, 
          {'title.4': {$ne: 0}}, 
          {'title.5': {$ne: 0}}, 
          {'title.6': {$ne: 0}}, 
          {'title.7': {$ne: 0}}, 
          {'title.8': {$ne: 0}}, 
          {'title.9': {$ne: 0}}, 
          {'title.10': {$ne: 0}}
        ],
      },
      {
        'title.0': 0, 
        'title.1': 0, 
        'title.2': 0, 
        'title.3': 0, 
        'title.4': 0, 
        'title.5': 0, 
        'title.6': 0, 
        'title.7': 0, 
        'title.8': 0, 
        'title.9': 0, 
        'title.10': 0,
        $or : [
          {'thumbnail.0': {$ne: 0}}, 
          {'thumbnail.1': {$ne: 0}}, 
          {'thumbnail.2': {$ne: 0}}, 
          {'thumbnail.3': {$ne: 0}}, 
          {'thumbnail.4': {$ne: 0}}, 
          {'thumbnail.5': {$ne: 0}}, 
          {'thumbnail.6': {$ne: 0}}, 
          {'thumbnail.7': {$ne: 0}}, 
          {'thumbnail.8': {$ne: 0}}, 
          {'thumbnail.9': {$ne: 0}}, 
          {'thumbnail.10': {$ne: 0}}
        ],
      }
    ]
  })

  res.send(readyToSend)
})

app.listen(8000)
console.log("Running...")

//Boolean:

