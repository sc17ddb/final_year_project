import numpy as np
import cv2
import glob
import seaborn as sns
from keras.layers.convolutional import Conv2D
from keras.optimizers import Adam
from keras.layers import Dense
from keras.layers.convolutional import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Activation
from keras.layers import BatchNormalization
from keras.layers import Dropout
from keras import Input
from keras.metrics import RootMeanSquaredError
from tensorflow.keras.models import Sequential
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt

images = []

attributes = pd.read_csv("subset_merged.csv")

# Model Architecture
model = Sequential()
model.add(Input(shape=(120, 120, 1)))
model.add(Conv2D(16, (3,3), padding='same', activation='relu'))
model.add(BatchNormalization(-1))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(32, (3,3), padding='same', activation='relu'))
model.add(BatchNormalization(-1))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
model.add(BatchNormalization(-1))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Flatten())
model.add(Dense(16, activation='relu'))
model.add(BatchNormalization(-1))
model.add(Dense(4, activation='relu'))
model.add(Dense(1, activation='linear'))

count = 0

# Load in images
for x in attributes['id']:
    print("{0}/{1}".format(count, len(attributes['id'])))
    image = cv2.resize(cv2.imread("../data_scraping/datasets/images/{}.jpg".format(x)), (120,120), interpolation = cv2.INTER_AREA)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    images.append(gray/255)
    count = count + 1

# Format data to ML usable
y = attributes['success_rating']

sns.distplot(y).get_figure().savefig("dist_y.png")
y_max = y.max()
y = y / y_max
X = np.array(images)
X = X.reshape(list(X.shape) + [1])

(X_train, X_test, y_train, y_test) = train_test_split(X, y, test_size = 0.2, random_state=79)

# Run the model
opt = Adam(learning_rate=0.001)
model.compile(optimizer=opt, loss='mean_absolute_percentage_error')
print("Training model")
model_hist = model.fit(X_train, y_train, validation_split=0.2, epochs=50)

# summarize history for loss
plt.plot(model_hist.history['loss'])
plt.plot(model_hist.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig("Loss_over_epochs_success.png")

# Predict and give a test loss
predictions = model.predict(X_test)
diff = predictions.flatten() - y_test
pd = (diff / y_test) * 100
apd = np.abs(pd)
mean = np.mean(apd)
print("mean absolute percentage difference: {}%".format(mean))