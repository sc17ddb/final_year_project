module.exports = {
	devServer: {
		disableHostCheck: true
	},
	chainWebpack: config => {
        config
        .module
        .rule("csv")
        .test(/\.csv$/)
        .use("csv-loader")
        .loader("csv-loader")
        .options({
          dynamicTyping: true,
          header: true,
          skipEmptyLines: true
        })
        .end()
    }
  }
