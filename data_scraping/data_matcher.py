import pandas as pd

video_id_scraper_csv = pd.read_csv("video_id_scraper_output.csv")

extra_data_scraper_csv = pd.read_csv("extra_data_scraper_output_fixed.csv")

final_file = open("data_matcher_output.csv", "w", encoding='utf-8')

video_id_scraper_csv.merge(extra_data_scraper_csv, on='id').to_csv("data_matcher_output.csv", index=False)